package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		sc= new Scanner (System.in);
		ArrayList lista = new ArrayList();
		System.out.println("ingrese el numero de jugadores");
		int numJugadores = Integer.parseInt(sc.next());
		for(int i = 0; i < numJugadores; i++)
		{
			System.out.println("ingrese el nombre del jugador" + ( i+1));
			String nombre = sc.next();
			System.out.println("ingrese el simbolo del jugador" + ( i+1));
			String simbolo = sc.next();
			lista.add(new Jugador(nombre,simbolo));
		}
		System.out.println( "ingrese el numero de filas");
		int numeroFilas = Integer.parseInt(sc.next());
		System.out.println( "ingrese el numero de columnas");
		int numeroColumnas = Integer.parseInt(sc.next());
		if( numeroFilas < 4 || numeroColumnas < 4 )
		{
			System.out.print("el numero de filas y el numero de columnas deben ser iguales o mayores a 4" );
			System.out.println( "ingrese el numero de filas");
			int numFilas = Integer.parseInt(sc.next());
			System.out.println( "ingrese el numero de columnas");
			int numColumnas = Integer.parseInt(sc.next());
			juego = new LineaCuatro(lista, numFilas, numColumnas);
			imprimirTablero();
			juego();
		}
		else 
		{
			juego = new LineaCuatro(lista, numeroFilas, numeroColumnas);
			juego();
			imprimirTablero();
		}
		//TODO
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		//TODO
		boolean termino = juego.fin();
		sc= new Scanner (System.in);
		String[][] tablero = juego.darTablero();
		while(!termino)
		{
			System.out.println("turno de: " + juego.darAtacante() + " ingrese el numero de una columna");
			int numero = Integer.parseInt(sc.next());
			if(numero > tablero[0].length -1 || numero < 0)
			{
				System.out.println("El numero no es valido vuelva a insertar un numero valido");
				int num = Integer.parseInt(sc.next());
				juego.registrarJugada(num);
				termino = juego.fin();
			}
			else
			{
				juego.registrarJugada(numero);
				termino = juego.fin();
			}
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		//TODO
		sc= new Scanner (System.in);
		ArrayList lista = new ArrayList();
		for(int i = 0; i < 2; i++)
		{
			if(i == 0)
			{
				System.out.println("ingrese el nombre del jugador" + ( i+1));
				String nombre = sc.next();
				System.out.println("ingrese el simbolo del jugador" + ( i+1));
				String simbolo = sc.next();
				if(simbolo.equals("x"))
				{
					System.out.println("ingrese un simbolo diferente a x puesto que este es el de la maquina");
					simbolo = sc.next();
				}
				lista.add(new Jugador(nombre,simbolo));
			}
			else
			{
				lista.add(new Jugador("maquina", "x" ));
			}
		}
		System.out.println( "ingrese el numero de filas");
		int numeroFilas = Integer.parseInt(sc.next());
		System.out.println( "ingrese el numero de columnas");
		int numeroColumnas = Integer.parseInt(sc.next());
		if( numeroFilas < 4 || numeroColumnas < 4 )
		{
			System.out.print("el numero de filas y el numero de columnas deben ser iguales o mayores a 4" );
			System.out.println( "ingrese el numero de filas");
			int numFilas = Integer.parseInt(sc.next());
			System.out.println( "ingrese el numero de columnas");
			int numColumnas = Integer.parseInt(sc.next());
			juego = new LineaCuatro(lista, numFilas, numColumnas);
			imprimirTablero();
			juegoMaquina();
		}
		else 
		{
			imprimirTablero();
			juego = new LineaCuatro(lista, numeroFilas, numeroColumnas);
			juegoMaquina();
		}
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		//TODO
		boolean termino = juego.fin();
		sc= new Scanner (System.in);
		String[][] tablero = juego.darTablero();
		while(!termino)
		{
			System.out.println("turno de: " + juego.darAtacante() + " ingrese el numero de una columna");
			int numero = Integer.parseInt(sc.next());
			if(numero > tablero[0].length -1 || numero < 0)
			{
				System.out.println("El numero no es valido vuelva a insertar un numero valido");
				int num = Integer.parseInt(sc.next());
				if(juego.darAtacante().equals("maquina"))
				{
					juego.registrarJugadaAleatoria();
					termino = juego.fin();
				}
				else
				{
					juego.registrarJugada(num);
					termino = juego.fin();
				}
			}
			else
			{
				if(juego.darAtacante().equals("maquina"))
				{
					juego.registrarJugadaAleatoria();
					termino = juego.fin();
				}
				else
				{
					juego.registrarJugada(numero);
					termino = juego.fin();
				}
			}
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] tablero = juego.darTablero();
		String a = "";
		for(int i = 0; i < tablero.length; i++)
		{
			for(int j = 0; j < tablero[0].length; j++)
			{
				System.out.println("|_|");
			}
		}
		//TODO
	}
}
