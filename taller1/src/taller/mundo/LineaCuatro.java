package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		Random random = new Random();
		int columna = random.nextInt(tablero[0].length-1);
		for(int i = 0; i < tablero.length - 1 ; i++)
		{
			if(tablero[i][columna].equals("___"))
			{
				tablero[i][columna]="x";
			}
		}

	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		boolean puede = false;
		boolean rpta = false;
		String s = "";
		for(int i = 0; i < jugadores.size(); i++ )
		{
			Jugador n = jugadores.get(i);
			if(atacante.equals(n.darNombre()))
			{
				s = n.darSimbolo();
			}
		}

		for(int i = 0; i< tablero.length && !puede ; i++)
		{
			if(tablero[i][col].equals("___"));
			puede = true;
		}
		if(puede)
		{
			for(int j = 0; j <= tablero[col].length - 1; j++)
			{
				if(tablero[j][col].equals("___"))
				{
					tablero[j][col] = s;
					rpta = true;
					turno ++;
				}
			}
		}
		return rpta;
		//TODO
	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		String s = "";
		for(int i = 0; i < jugadores.size(); i++ )
		{
			Jugador n = jugadores.get(i);
			if(atacante.equals(n.darNombre()))
			{
				s = n.darSimbolo();
			}
		}
		if(tablero[fil][col + 1].equals(s) && tablero[fil][col + 2].equals(s) && tablero[fil][col + 3].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil][col - 1].equals(s) && tablero[fil][col + 1].equals(s) && tablero[fil][col + 2].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil][col - 2].equals(s) && tablero[fil][col  -1].equals(s) && tablero[fil][col + 1].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil][col - 3].equals(s) && tablero[fil][col  -2].equals(s) && tablero[fil][col -1].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil + 1][col].equals(s) && tablero[fil + 2][col].equals(s) && tablero[fil + 3][col].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil  -1][col].equals(s) && tablero[fil + 1 ][col].equals(s) && tablero[fil + 2][col].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil  -2][col].equals(s) && tablero[fil - 1 ][col].equals(s) && tablero[fil + 1][col].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil  -3][col].equals(s) && tablero[fil - 2 ][col].equals(s) && tablero[fil - 1][col].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil + 1][col + 1].equals(s) && tablero[fil + 2 ][col + 2].equals(s) && tablero[fil +3 ][col + 3].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil - 1][col - 1].equals(s) && tablero[fil + 1 ][col + 1].equals(s) && tablero[fil +2 ][col + 2].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil - 2][col - 2].equals(s) && tablero[fil - 1 ][col - 1].equals(s) && tablero[fil +1 ][col + 1].equals(s))
		{
			finJuego = true;
		}
		else if(tablero[fil - 3][col - 3].equals(s) && tablero[fil - 2 ][col - 2].equals(s) && tablero[fil -1 ][col - 1].equals(s))
		{
			finJuego = true;
		}
		//TODO
		return finJuego;
	}



}
